package cs0ip.eventmanager

trait Stoppable {
  protected def onStop(): Unit = ()

  @volatile
  private var _isStopped: Boolean = false
  def isStopped: Boolean = _isStopped
  def stop(): Unit = {
    _isStopped = true
    onStop()
  }

  def waitUntilStop(timeout: Long = 0): Unit

  def stopAndWait(timeout: Long = 0): Unit = {
    stop()
    waitUntilStop(timeout)
  }
}

trait StoppableWithSingleThread extends Stoppable {
  protected def threadToStop: Thread

  def waitUntilStop(timeout: Long = 0): Unit =
    threadToStop.join(timeout)
}
