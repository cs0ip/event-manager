package cs0ip.eventmanager

class SleepLock {
  @volatile private var _readyToSleep: Boolean = false
  def readyToSleep: Boolean = _readyToSleep

  def tryToSleep(timeout: Long = 0): Unit = {
    this.synchronized {
      if (readyToSleep) {
        val wakeUpTime: Long =
          if (timeout > 0) System.currentTimeMillis() + timeout
          else 0
        while (readyToSleep) {
          this.wait(timeout)
          if (readyToSleep && wakeUpTime > 0) {
            val time = System.currentTimeMillis()
            if (time >= wakeUpTime)
              _readyToSleep = false
          }
        }
      } else
        _readyToSleep = true
    }
  }

  def wakeUp(): Unit = {
    if (readyToSleep) {
      this.synchronized {
        if (readyToSleep) {
          _readyToSleep = false
          this.notifyAll()
        }
      }
    }
  }
}
