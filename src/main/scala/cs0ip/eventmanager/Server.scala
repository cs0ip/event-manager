package cs0ip.eventmanager

import java.lang.ref.WeakReference
import java.util
import java.util.concurrent.ConcurrentLinkedQueue

private[eventmanager]
class Server(manager: Manager.ServerOps) extends StoppableWithSingleThread {
  import Server._

  private val clients = new util.LinkedList[ClientNode]
  private val clientsToAdd = new ConcurrentLinkedQueue[ClientNode]

  private val sleepLock = new SleepLock

  private val thread = new Thread(
    new Runnable {
      def processQueue(queue: ConcurrentLinkedQueue[Event]): Boolean = {
        var isEmpty = true
        var event = queue.poll()
        while (event != null) {
          isEmpty = false
          manager.add(event)
          event = queue.poll()
        }
        isEmpty
      }

      def processClients(): Boolean = {
        var activity = false
        val it = clients.iterator()
        while (it.hasNext) {
          val node = it.next()
          val queue = node.queue
          val isEmpty = processQueue(queue)
          activity ||= ! isEmpty
          if (isEmpty) {
            if (node.readyToDelete)
              it.remove()
            else {
              val client = node.clientRef.get()
              if (client == null || client.isClosed) {
                node.readyToDelete = true
                activity = true
              }
            }
          }
        }
        activity
      }

      def processClientsToAdd(): Boolean = {
        var activity = false
        var node = clientsToAdd.poll()
        while (node != null) {
          activity = true
          clients.add(node)
          node = clientsToAdd.poll()
        }
        activity
      }

      def run(): Unit = {
        while (! isStopped) {
          val clientsActivity = processClients()
          val clientsToAddActivity = processClientsToAdd()
          if (! (clientsActivity || clientsToAddActivity))
            sleepLock.tryToSleep()
        }
      }
    },
    "eventmanager-server"
  )

  protected def threadToStop: Thread = thread
  override protected def onStop(): Unit =
    wakeUp()

  def createClient(): Client = {
    if (isStopped)
      throw new IllegalStateException("server is stopped")
    val queue = new ConcurrentLinkedQueue[Event]
    val client = new Client(queue, this)
    val clientRef = new WeakReference(client)
    val node = new ClientNode(clientRef, queue)
    clientsToAdd.add(node)
    wakeUp()
    client
  }

  def wakeUp(): Unit =
    sleepLock.wakeUp()

  thread.setDaemon(true)
  thread.start()
}

object Server {
  private[eventmanager]
  class ClientNode(
    val clientRef: WeakReference[Client],
    val queue: ConcurrentLinkedQueue[Event]
  ) {
    var readyToDelete: Boolean = false
  }
}
