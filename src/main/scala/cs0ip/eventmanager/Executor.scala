package cs0ip.eventmanager

import java.util.function.UnaryOperator

import cs0ip.eventmanager.Manager.{CurrentEvent, MapEntry}

private[eventmanager]
class Executor(events: Manager.Events, ceRef: Manager.CurrentEventRef) extends StoppableWithSingleThread {
  import Executor._

  private val sleepLock = new SleepLock

  private val thread = new Thread(
    new Runnable {
      import CurrentEvent.State

      def getCurrentEvent(ignore: CurrentEvent = null): CurrentEvent = {
        val refEvent: CurrentEvent = ceRef.get()
        if ((refEvent eq ignore) || refEvent == null || refEvent.state != State.Checked) {
          var res: CurrentEvent = null
          do {
            val mapEvent = events.firstEntry()
            res = ceRef.updateAndGet(new RefUpdater(mapEvent, ignore))
          } while (! (res == null || res.state == State.Checked))
          res
        } else
          refEvent
      }

      def run(): Unit = {
        var ignore: CurrentEvent = null
        while (! isStopped) {
          val currentEvent = getCurrentEvent(ignore)
          ignore = null
          if (currentEvent == null)
            sleepLock.tryToSleep()
          else {
            val entry = currentEvent.entry
            val key = entry.getKey
            val currentTime = System.currentTimeMillis()
            if (currentTime >= key.time) {
              entry.getValue.run()
              events.remove(key)
              ignore = currentEvent
            } else {
              val diff = key.time - currentTime
              sleepLock.tryToSleep(diff)
            }
          }
        }
      }
    },
    "eventmanager-executor"
  )

  protected def threadToStop: Thread = thread
  override protected def onStop(): Unit =
    wakeUp()

  def wakeUp(): Unit =
    sleepLock.wakeUp()

  thread.setDaemon(true)
  thread.start()
}


object Executor {
  private[eventmanager]
  class RefUpdater(entry: MapEntry, ignore: CurrentEvent) extends UnaryOperator[CurrentEvent] {
    import CurrentEvent.State

    private var _currentEvent: CurrentEvent = _
    def currentEvent: CurrentEvent = {
      if (entry == null)
        null
      else {
        if (_currentEvent == null)
          _currentEvent = CurrentEvent(entry, CurrentEvent.State.Checked)
        _currentEvent
      }
    }

    def apply(old: CurrentEvent): CurrentEvent = {
      if ((old eq ignore) || old == null) {
        currentEvent
      } else if (old.state == State.New) {
        if (entry == null || old.entry.getKey.time < entry.getKey.time)
          old.copy(state = State.Seen)
        else
          currentEvent
      } else if (old.state == State.Seen) {
        currentEvent
      } else {
        throw new IllegalStateException(s"state = ${old.state}")
      }
    }
  }
}
