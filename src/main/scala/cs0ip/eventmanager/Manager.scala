package cs0ip.eventmanager

import java.util.concurrent.ConcurrentSkipListMap
import java.util.concurrent.atomic.AtomicReference
import java.util.function.UnaryOperator

class Manager extends Stoppable {
  import Manager._

  private val events = new Events
  private val ceRef = new CurrentEventRef(null)
  private val executor = new Executor(events, ceRef)
  private val serverOps = new ServerOps(events, ceRef, executor)
  private val server = new Server(serverOps)

  def createClient(): Client = server.createClient()

  override protected def onStop(): Unit = {
    server.stop()
    executor.stop()
  }

  def waitUntilStop(timeout: Long = 0): Unit = {
    val t1 = if (timeout == 0) 0 else System.currentTimeMillis()
    server.waitUntilStop(timeout)
    if (timeout == 0)
      executor.waitUntilStop()
    else {
      val t2 = System.currentTimeMillis()
      val timeout2 = timeout - (t2 - t1)
      if (timeout2 > 0)
        executor.waitUntilStop(timeout2)
    }
  }
}

object Manager {
  private[eventmanager]
  case class EventKey(time: Long, ord: Int) extends Ordered[EventKey] {
    def compare(that: EventKey): Int = {
      val timeCmp = this.time.compareTo(that.time)
      if (timeCmp == 0)
        java.lang.Integer.compare(this.ord, that.ord)
      else
        timeCmp
    }
  }

  private[eventmanager]
  type Events = ConcurrentSkipListMap[EventKey, Runnable]

  private[eventmanager]
  type MapEntry = java.util.Map.Entry[EventKey, Runnable]

  private[eventmanager]
  case class CurrentEvent(entry: MapEntry, state: CurrentEvent.State)

  object CurrentEvent {
    sealed trait State

    object State {
      object New extends State
      object Seen extends State
      object Checked extends State
    }
  }

  private[eventmanager]
  type CurrentEventRef = AtomicReference[CurrentEvent]

  private[eventmanager]
  class ServerOps(events: Events, ceRef: CurrentEventRef, executor: Executor) {
    import ServerOps._

    def add(event: Event): Unit = {
      val prevKey = events.floorKey(EventKey(event.time, Int.MaxValue))
      val ord = if (prevKey == null) 0 else prevKey.ord + 1
      val key = EventKey(event.time, ord)
      events.put(key, event.task)
      val updater = new RefUpdater(key, event.task)
      ceRef.updateAndGet(updater)
      if (updater.needNotify)
        executor.wakeUp()
    }
  }

  private[eventmanager]
  object ServerOps {
    private def newMapEntry(key: EventKey, value: Runnable): MapEntry = {
      new MapEntry {
        def getKey: EventKey = key
        def getValue: Runnable = value
        def setValue(value: Runnable): Runnable =
          throw new UnsupportedOperationException
      }
    }

    private class RefUpdater(key: EventKey, task: Runnable) extends UnaryOperator[CurrentEvent] {
      private var _needNotify: Boolean = false
      def needNotify: Boolean = _needNotify

      private var _currentEvent: CurrentEvent = _
      def currentEvent: CurrentEvent = {
        if (_currentEvent == null)
          _currentEvent = CurrentEvent(newMapEntry(key, task), CurrentEvent.State.New)
        _currentEvent
      }

      def apply(old: CurrentEvent): CurrentEvent = {
        //здесь не нужно сравнивать ord, т.к. чем позже добавлено событие, тем больше у него ord
        //при одинаковом time
        if (old == null || key.time < old.entry.getKey.time) {
          _needNotify = true
          currentEvent
        } else {
          _needNotify = false
          old
        }
      }
    }
  }
}
