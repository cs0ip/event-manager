package cs0ip.eventmanager

import java.time.{LocalDateTime, ZoneId}

case class Event(time: Long, task: Runnable)

object Event {
  def apply(time: LocalDateTime, task: Runnable): Event =
    Event(time.atZone(ZoneId.systemDefault()).toInstant.toEpochMilli, task)
}
