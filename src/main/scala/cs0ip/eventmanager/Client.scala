package cs0ip.eventmanager

import java.util.concurrent.ConcurrentLinkedQueue

class Client(queue: ConcurrentLinkedQueue[Event], server: Server) extends AutoCloseable {
  @volatile
  private var _isClosed: Boolean = false
  def isClosed: Boolean = _isClosed
  def close(): Unit =
    _isClosed = true

  def addEvent(event: Event): Unit = {
    if (isClosed)
      throw new IllegalStateException("client is closed")
    val isEmpty = queue.isEmpty
    queue.add(event)
    if (isEmpty)
      server.wakeUp()
  }
}
