import java.util.concurrent.CountDownLatch

import cs0ip.eventmanager._

class Tests extends BaseSuite {
  test("many clients") {
    class Test extends Runnable {
      val clientsCnt = 10000
      val msgCnt = 1000
      val manager = new Manager
      val latch = new CountDownLatch(clientsCnt)

      @volatile
      var counter: Int = 0

      class Run extends Runnable {
        def run(): Unit = {
          val client = manager.createClient()
          latch.countDown()
          latch.await()
          val time = System.currentTimeMillis()
          for (ind <- 0 until msgCnt) {
            val r = new Runnable {
              @volatile var executed = false
              def run(): Unit = {
                assert(! executed)
                counter += 1
                executed = true
              }
            }
            client.addEvent(Event(time + ind % 10 * 100, r))
          }
        }
      }

      def run(): Unit = {
        for (_ <- 0 until clientsCnt) {
          val thread = new Thread(new Run)
          thread.start()
        }

        val cnt = clientsCnt * msgCnt
        var prevCounter = 0
        var prevTime: Long = System.currentTimeMillis()
        val sleepTime = 3000
        //noinspection LoopVariableNotUpdated
        while (counter < cnt) {
          Thread.sleep(sleepTime)
          val curCounter = counter
          val curTime = System.currentTimeMillis()
          val rate = (curCounter - prevCounter) / ((curTime - prevTime) / 1000)
          println(s"counter = ${counter}; rate = ${rate}/s")
          prevCounter = curCounter
          prevTime = curTime
        }

        Thread.sleep(1000)
        println(s"counter = ${counter}")

        manager.stop()

        counter shouldBe cnt
      }
    }
    println("start")
    (new Test).run()
  }
}
