import org.scalatest._

trait BaseSuite
  extends FunSuite
  with Matchers
  with OptionValues
  with Inside
  with Inspectors
  with CancelAfterFailure
